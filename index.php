﻿<!DOCTYPE html>
<html>

<!--[if lte IE 5]>
<script>
    window.location = "http://outdatedbrowser.com/en";
</script>
<![endif]-->
<!--[if lte IE 6]>
<script>
    window.location = "http://outdatedbrowser.com/en";
</script>
<![endif]-->
<!--[if lte IE 7]>
<script>
    window.location = "http://outdatedbrowser.com/en";
</script>
<![endif]-->
<!--[if lte IE 8]>
<script>
    window.location = "http://outdatedbrowser.com/en";
</script>
<![endif]-->

<head>
    <meta charset="UTF-8">
    <title>رستوران ایکس</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="fonts/IRANSans/css/fontiran.css">
    <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link href="css/browser-reset.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link rel="icon" href="favicon.png" type="image/x-icon">
    <meta name="description" content="">
</head>

<body>
<!-- Navigation -->
<nav class="nav">
    <div class="wrapper">
        <a href="#" class="nav__logo">
            <img src="images/logo.png" alt="Logo">
            <span class="nav__logo__name">رستوران ایکس</span>
            <span>طعم های ناشناخته</span>
        </a>
        <ul class="navigation">
            <li><a href="#">خانه</a></li>
            <li><a href="#"> محصولات<i class="fa fa-angle-down"></i></a>
                <ul>
                    <li><a href="#">محصول 1</a></li>
                    <li><a href="#">محصول 2</a></li>
                    <li><a href="#">محصول 3</a></li>
                    <li><a href="#">محصول 4</a></li>
                </ul>
            </li>
            <li><a href="#">درباره</a></li>
            <li><a href="#">تماس</a></li>
        </ul>
        <div class="menu-btn">
            <span class="menu-btn__line"></span>
            <span class="menu-btn__line"></span>
            <span class="menu-btn__line"></span>
        </div>
        <a class="nav__telephone Fa" href="tel:021750440">
            <span>تلفن سفارشات</span>
            <span>021 <span class="nav__telephone__big-text"> 750 440</span></span>
        </a>
    </div>
</nav>
<!-- Slider -->
<div class="fpslider" id="slider_id">

    <ul class="fpslider__bg">
        <li class="fpslider__bg-item" style="background-image: url('images/slide1.jpg');"></li>
        <li class="fpslider__bg-item" style="background-image: url('images/slide2.jpg');"></li>
        <li class="fpslider__bg-item" style="background-image: url('images/slide3.jpg');"></li>
    </ul>

    <ul class="fpslider__content">
        <li class="fpslider__content-item">
            <h2 class="fpslider__title">طعم‌های ناشناخته را با ما تجربه کنید </h2>
            <h3 class="fpslider__subtitle">۱۵ سال همراه‌تان بوده‌ایم</h3>
        </li>
        <li class="fpslider__content-item">
            <h2 class="fpslider__title">طعم‌های ناشناخته را با ما تجربه کنید</h2>
            <h3 class="fpslider__subtitle">۱۵ سال همراه‌تان بوده‌ایم</h3>
        </li>
        <li class="fpslider__content-item">
            <h2 class="fpslider__title">طعم‌های ناشناخته را با ما تجربه کنید</h2>
            <h3 class="fpslider__subtitle">۱۵ سال همراه‌تان بوده‌ایم</h3>
        </li>
    </ul>

    <div class="fpslider__nav">
        <ul class="fpslider__bullet"></ul>
    </div>

</div>



<!-----------------------------------
------------== Scripts ==------------
------------------------------------>

<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/velocity.min.js"></script>
<script src="js/velocity.ui.min.js"></script>
<script src="js/persianumber.js"></script>
<script src="js/fpslider.js"></script>

<script src="js/scripts.js"></script>
</body>
</html>