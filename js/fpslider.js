// full page slider
(function($) {
	$.fn.fpslider = function(options){

		var setting = $.extend({
			image: '.fpslider__bg-item', // Slide image item selector
			content: '.fpslider__content-item', // Slide text/title item selector
			content_wrap: '.fpslider__content', // Slide content wrap
			bullets_wrapper: '.fpslider__bullet', // Bullets wrapper selector
			btn_next: '.fpslider__next',
			btn_prev: '.fpslider__prev',
			interval: 2000
		},options);

		this.each(function(){
			var $slider = $(this),
					$img = $slider.find(setting.image),
					$content = $slider.find(setting.content),
					$contentWrap = $slider.find(setting.content_wrap),
					$bltwrp = $slider.find(setting.bullets_wrapper),
					$bullet,
					$btnNext = $slider.find(setting.btn_next),
					$btnPrev = $slider.find(setting.btn_prev),

					interval = setting.interval,
					count = $img.length,
					current = 0,
					timer;

			init();
			rotate(true);

			function init(){
				$img.each(function(){
					$bltwrp.append('<span></span>');
				});
				$bullet = $bltwrp.find('span');

				$img.eq(0).addClass('active');
				$content.eq(0).addClass('active');
				$bullet.eq(0).addClass('active');

				if($bltwrp.find('span').length <=1){
					$bltwrp.css({'display':'none'});
				}

				// $contentWrap.height($content.eq(0).innerHeight());
			}

			function forward() {
				if(current >= count){
					current = 0;
				}
				else if(current < 0){
					current = count - 1;
				}

				$img.removeClass('active');
				$content.removeClass('active');
				$bullet.removeClass('active');
				$img.eq(current).addClass('active');
				$content.eq(current).addClass('active');
				$bullet.eq(current).addClass('active');

				// $contentWrap.height($content.eq(current).innerHeight());

				rotate(true);
				console.log('forwarding');
			}

			function rotate(stat) {
				if(stat){
					timer = setTimeout(function(){
						current = current + 1;
						forward();
					},interval);
				}
				else{
					clearTimeout(timer);
				}
			}

			$bullet.click(function() {
				//disable rotate
				rotate(false);
				//which pagination btn has been clicked
				current = $(this).index();
				//play slider
				$img.removeClass('active');
				$content.removeClass('active');
				$bullet.removeClass('active');
				$img.eq(current).addClass('active');
				$content.eq(current).addClass('active');
				$bullet.eq(current).addClass('active');
			});

			$btnNext.click(function(){
				//disable rotate
				rotate(false);
				//increment current
				current += 1;
				//play slider
				forward();
			});

			$btnPrev.click(function(){
				//disable rotate
				rotate(false);
				//increment current
				current -= 1;
				//play slider
				forward();
			});

			// pause slider on hover
			if(windowWidth > SCR_MD_MIN){
				$contentWrap.hover(function(){
					rotate(false);
				},function(){
					rotate(true);
				});
				$bltwrp.hover(function(){
					rotate(false);
				},function(){
					rotate(true);
				});
			}

		});

		return this;
	};
})(jQuery);