
/*-----------------------------------
 -------== Disable Img Select ==------
 ---------------------------------- */

$(document).ready(function () {
    $('img').mousedown(function () {
        return false;
    });
});

/*-----------------------------------
--------== Persian Numbers ==--------
---------------------------------- */

$(document).ready(function(){
	$('.Fa').persiaNumber('fa');
});

/*-----------------------------------
----------== Navigation  ==----------
---------------------------------- */

$(document).ready(function(){
    $(document).on("click",'.menu-btn', function () {
        openClose();
    });
    $(document).on("click",'nav .navigation li a', function () {
        if (Modernizr.mq('(max-width: 767px)')) {
            $(this).parent().find('>ul').stop().slideToggle();
        }
    });
});
/* Open Close Mobile Menu */
var check = 0;
function openClose(){
    if( check==0){
        $('.menu-btn__line:nth-of-type(1)').addClass('mobileBars45');
        $('.menu-btn__line:nth-of-type(3)').css({'opacity':'0'});
        $('.menu-btn__line:nth-of-type(2)').addClass('mobileBars45B');
        $('nav').addClass('active');
        if(!$('.nav__telephone').is(':visible')){
            $('.nav__telephone,.navigation').velocity("stop", true).delay(400).velocity("fadeIn", { duration: 300 });
        }
        check = 1;
    }else{
        $('.menu-btn__line:nth-of-type(1)').removeClass('mobileBars45');
        $('.menu-btn__line:nth-of-type(3)').css({'opacity':'1'});
        $('.menu-btn__line:nth-of-type(2)').removeClass('mobileBars45B');
        $('nav').removeClass('active');
        $('.nav__telephone,.navigation ul').hide();
        $('.navigation').velocity("stop", true).velocity("fadeOut", { duration: 100 });
        setTimeout(function () {
            check = 0;
        },1200);
    }
}
/* Ckeck If It Is Desktop */
$(window).on('load resize', function() {
    if (Modernizr.mq('(min-width: 768px)')) {
        $('nav').removeClass('active');
        $('.menu-btn__line:nth-of-type(1)').removeClass('mobileBars45');
        $('.menu-btn__line:nth-of-type(3)').css({'opacity':'1'});
        $('.menu-btn__line:nth-of-type(2)').removeClass('mobileBars45B');
        $('.nav__telephone').show().css({'display':'inline-block'});
        $('.navigation,.navigation ul').stop().show().css({'opacity':'1'});
        check = 0;
    }else{
        if(!$('nav').hasClass('active')){
            $('.nav__telephone,.navigation ul,.navigation').css({'display':'none'});
        }
    }
});
/* Slider */
var windowWidth = window.innerWidth
        || document.documentElement.clientWidth //IE <= 8
        || document.body.clientWidth,

    windowHeight = window.innerHeight
        || document.documentElement.clientHeight //IE <= 8
        || document.body.clientHeight,

    requestAnimationFrame = window.requestAnimationFrame
        || window.mozRequestAnimationFrame
        || window.webkitRequestAnimationFrame
        || window.msRequestAnimationFrame,

    cancelAnimationFrame = window.cancelAnimationFrame
        || window.mozCancelAnimationFrame,

    // media query breakpoints
    SCR_XS_MAX = 500,
    SCR_SM_MIN = 501,
    SCR_SM_MAX = 800,
    SCR_MD_MIN = 801,
    SCR_MD_MAX = 1050,
    SCR_LG_MIN = 1051,
    SCR_LG_MAX = 1440,
    SCR_XLG_MIN = 1441;

$('#slider_id').fpslider();